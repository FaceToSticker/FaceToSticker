import sqlite3
conn = sqlite3.connect('log.db')
conn.execute('''CREATE TABLE chats
         (chatid INT PRIMARY KEY     NOT NULL,
         lastusage           INT    NOT NULL);''')

conn.execute('''CREATE TABLE stickers
         (id INTEGER PRIMARY KEY  AUTOINCREMENT   NOT NULL,
         chatid INT NOT NULL,
         fileid INT NOT NULL,
         kept BOOl NOT NULL,
         time INT NOT NULL);''')

conn.close()