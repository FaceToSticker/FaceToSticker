import yaml

class TelegramClass:
    def __init__(self, yaml_raw):
        self.defaultStickerSetName = yaml_raw["defaultStickerSetName"]
        self.botToken = yaml_raw["botToken"]
        self.admins = yaml_raw["admins"]


class StatisticsClass:
    def __init__(self, yaml_raw):
        self.dbType = yaml_raw["dbType"]
        self.filename = yaml_raw["filename"]


class Config:
    def __init__(self):
        with open("conf.yaml", 'r') as stream:
            try:
                yaml_raw = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
        self.logLevel = yaml_raw["logLevel"]
        self.statistics = StatisticsClass(yaml_raw["Statistics"])
        self.telegram = TelegramClass(yaml_raw["Telegram"])
