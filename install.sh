#!/usr/bin/env bash

echo "creating the service ..."
cp sticker.sh /etc/init.d/sticker
cp sticker.service /etc/systemd/system
chmod +x /etc/init.d/sticker
# stream editor
sed -i "s/git/facetosticker/bot.py/g" /etc/init.d/sticker
echo "created the service sticker"

